import { IsInt, IsNotEmpty, MinLength } from 'class-validator';

export class CreateProductDto {
  @MinLength(5)
  @IsNotEmpty()
  name: string;

  @IsInt()
  @IsNotEmpty()
  price: number;
}
